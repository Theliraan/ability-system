// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <Abilities/GameplayAbilityTargetActor.h>

#include "GATargetGetAround.generated.h"

/**
 * 
 */
UCLASS()
class ABILITYSYSTEM_API AGATargetGetAround : public AGameplayAbilityTargetActor
{
	GENERATED_BODY()

public:

	virtual void StartTargeting(class UGameplayAbility* Ability) override;
	virtual void ConfirmTargetingAndContinue() override;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(ExposeOnSpawn=true), Category="GetAround")
	float Radius;
	
};
