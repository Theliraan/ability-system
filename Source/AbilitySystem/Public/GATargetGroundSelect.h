// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <Abilities/GameplayAbilityTargetActor.h>
#include <CoreMinimal.h>

#include "GATargetGroundSelect.generated.h"

class UDecalComponent;
class USceneComponent;

/**
 * 
 */
UCLASS()
class ABILITYSYSTEM_API AGATargetGroundSelect : public AGameplayAbilityTargetActor
{
	GENERATED_BODY()

public:

	AGATargetGroundSelect();

	virtual void Tick(float DeltaSeconds) override;

	virtual void StartTargeting(class UGameplayAbility* Ability) override;
	virtual void ConfirmTargetingAndContinue() override;

	UFUNCTION(BlueprintCallable, Category="GroundSelect")
	bool GetPlayerLookPoint(FVector& LookAtPoint);

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(ExposeOnSpawn=true), Category="GroundSelect")
	float Radius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="GroundSelect")
	UDecalComponent* Decal;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="GroundSelect")
	USceneComponent* RootComp;
};
