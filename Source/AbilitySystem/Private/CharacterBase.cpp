// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterBase.h"

#include <AIController.h>
#include <AbilitySystemComponent.h>
#include <BrainComponent.h>
#include <GameFramework/PlayerController.h>

#include "GameplayAbilityBase.h"
#include "PlayerControllerBase.h"

#include "AttributeSetBase.h"

struct FGameplayAbilityInfo;

// Sets default values
ACharacterBase::ACharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>("AbilitySystemComponent");
	AttributeSetBaseComponent = CreateDefaultSubobject<UAttributeSetBase>("AttributeSetBaseComponent");
}

// Called when the game starts or when spawned
void ACharacterBase::BeginPlay()
{
	Super::BeginPlay();

	AttributeSetBaseComponent->OnHealthChange.AddDynamic(this, &ACharacterBase::OnHealthChanged);
	AttributeSetBaseComponent->OnManaChange.AddDynamic(this, &ACharacterBase::OnManaChanged);
	AttributeSetBaseComponent->OnStrengthChange.AddDynamic(this, &ACharacterBase::OnStrengthChanged);
	AddGameplayTag(FullHealthTag);

	TeamID = [&]()
	{
		if (const auto Controller = GetController())
		{
			if (Controller->IsPlayerController())
			{
				return 0;
			}
		}
		return 255;
	}();
}

// Called every frame
void ACharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ACharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ACharacterBase::AcquireAbility(TSubclassOf<UGameplayAbility> AbilityToAcquire)
{
	if (AbilitySystemComponent)
	{
		if (HasAuthority() && AbilityToAcquire)
		{
			AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(AbilityToAcquire, 1, 0));
		}
		AbilitySystemComponent->InitAbilityActorInfo(this, this);

		if (AbilityToAcquire->IsChildOf(UGameplayAbilityBase::StaticClass()))
		{
			if (TSubclassOf<UGameplayAbilityBase> AbilityBaseClass = *AbilityToAcquire)
			{
				AddAbilityToUI(AbilityBaseClass);
			}
		}
	}
}

void ACharacterBase::AcquireAbilities(TArray<TSubclassOf<UGameplayAbility>> AbilitiesToAcquire)
{
	for (TSubclassOf<UGameplayAbility> AbilityToAcquire : AbilitiesToAcquire) 
	{
		AcquireAbility(AbilityToAcquire);
	}
}

void ACharacterBase::OnHealthChanged(float Health, float MaxHealth)
{
	if (!bIsDead)
	{
		if (Health >= MaxHealth)
		{
			AddGameplayTag(FullHealthTag);
		}
		else
		{
			RemoveGameplayTag(FullHealthTag);
		}

		if (Health <= 0.f)
		{
			Die();
		}
		BP_OnHealthChanged(Health, MaxHealth);
	}
}

void ACharacterBase::OnManaChanged(float Mana, float MaxMana)
{
	if (!bIsDead)
	{
		BP_OnManaChanged(Mana, MaxMana);
	}
}

void ACharacterBase::OnStrengthChanged(float Strength, float MaxStrength)
{
	if (!bIsDead)
	{
		BP_OnStrengthChanged(Strength, MaxStrength);
	}
}

bool ACharacterBase::IsOtherHostile(ACharacterBase* Other)
{
	return IsValid(Other) && Other->TeamID != TeamID;
}

void ACharacterBase::Die()
{
	bIsDead = true;
	DisableAnyInput();
	BP_Die();
}

void ACharacterBase::DisableAnyInput()
{
	if (APlayerController* PC = Cast<APlayerController>(GetController()))
	{
		PC->DisableInput(PC);
	}
	else if (AAIController* AIC = Cast<AAIController>(GetController()))
	{
		AIC->GetBrainComponent()->StopLogic("Die");
	}
}

void ACharacterBase::EnableAnyInput()
{
	if (bIsDead)
	{
		return;
	}

	if (APlayerController* PC = Cast<APlayerController>(GetController()))
	{
		PC->EnableInput(PC);
	}
	else if (AAIController* AIC = Cast<AAIController>(GetController()))
	{
		AIC->GetBrainComponent()->RestartLogic();
	}
}

void ACharacterBase::AddGameplayTag(const FGameplayTag& TagToAdd)
{
	GetAbilitySystemComponent()->AddLooseGameplayTag(TagToAdd);
	GetAbilitySystemComponent()->SetTagMapCount(TagToAdd, 1); // Clamp to 1
}

void ACharacterBase::RemoveGameplayTag(const FGameplayTag& TagToRemove)
{
	GetAbilitySystemComponent()->RemoveLooseGameplayTag(TagToRemove);
	GetAbilitySystemComponent()->SetTagMapCount(TagToRemove, 0); // Clamp to 0
}

void ACharacterBase::AddAbilityToUI(TSubclassOf<UGameplayAbilityBase> AbilityToAdd)
{
	if (APlayerControllerBase* PlayerControllerBase = Cast<APlayerControllerBase>(GetController()))
	{
		if (UGameplayAbilityBase* AbilityInstance = AbilityToAdd.Get()->GetDefaultObject<UGameplayAbilityBase>())
		{
			FGameplayAbilityInfo AbilityInfo = AbilityInstance->GetAbilityInfo();
			PlayerControllerBase->AddAbilityToUI(AbilityInfo);
		}
	}
}