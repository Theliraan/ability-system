// Fill out your copyright notice in the Description page of Project Settings.


#include "GATargetGroundSelect.h"

#include <Abilities/GameplayAbility.h>
#include <Engine/World.h>
#include <GameFramework/Pawn.h>
#include <GameFramework/PlayerController.h>
#include <DrawDebugHelpers.h>
#include <Components/DecalComponent.h>
#include <Components/SceneComponent.h>

AGATargetGroundSelect::AGATargetGroundSelect()
{
    PrimaryActorTick.bCanEverTick = true;
    Decal = CreateDefaultSubobject<UDecalComponent>("Decal");
    RootComp = CreateDefaultSubobject<USceneComponent>("RootComp");

    SetRootComponent(RootComp);
    Decal->SetupAttachment(RootComp);
}

void AGATargetGroundSelect::StartTargeting(UGameplayAbility* Ability)
{
    Super::StartTargeting(Ability);
    MasterPC = Cast<APlayerController>(Ability->GetOwningActorFromActorInfo()->GetInstigatorController());
    Decal->DecalSize = FVector(Radius);
}

void AGATargetGroundSelect::ConfirmTargetingAndContinue()
{
    FVector ViewLocation;
    GetPlayerLookPoint(ViewLocation);

    TArray<FOverlapResult> Overlaps;
    TArray<TWeakObjectPtr<AActor>> OverlapedActor; 
    bool IsTraceComplex = false;

    FCollisionQueryParams CollisionQueryParams;
    CollisionQueryParams.bTraceComplex = IsTraceComplex;
    CollisionQueryParams.bReturnPhysicalMaterial = false;
    if (APawn* MasterPawn = MasterPC->GetPawn())
    {
        CollisionQueryParams.AddIgnoredActor(MasterPawn->GetUniqueID());
    }

    const bool IsOverlap = GetWorld()->OverlapMultiByObjectType(Overlaps, 
                                                                ViewLocation, 
                                                                FQuat::Identity, 
                                                                FCollisionObjectQueryParams(ECC_Pawn), 
                                                                FCollisionShape::MakeSphere(Radius), 
                                                                CollisionQueryParams);
    if (IsOverlap)
    {
        for (int32 o = 0; o < Overlaps.Num(); ++o)
        {
            if (APawn* OverlapedPawn = Cast<APawn>(Overlaps[o].GetActor()))
            {
                OverlapedActor.AddUnique(OverlapedPawn);
            }
        }
    }

    FGameplayAbilityTargetData_LocationInfo* CenterLocation = new FGameplayAbilityTargetData_LocationInfo();
    if (Decal)
    {
        CenterLocation->TargetLocation.LiteralTransform = Decal->GetComponentTransform();
        CenterLocation->TargetLocation.LocationType = EGameplayAbilityTargetingLocationType::LiteralTransform;
    }

    if (OverlapedActor.Num() > 0)
    {
        FGameplayAbilityTargetDataHandle TargetData = StartLocation.MakeTargetDataHandleFromActors(OverlapedActor);
        TargetData.Add(CenterLocation);
        TargetDataReadyDelegate.Broadcast(TargetData);
    }
    else
    {
        TargetDataReadyDelegate.Broadcast(FGameplayAbilityTargetDataHandle(CenterLocation));
    }
}

bool AGATargetGroundSelect::GetPlayerLookPoint(FVector& LookAtPoint)
{
    FVector ViewPoint;
    FRotator ViewRotation;
    MasterPC->GetPlayerViewPoint(ViewPoint, ViewRotation);

    FCollisionQueryParams QueryParams;
    QueryParams.bTraceComplex = true;
    if (APawn* MasterPawn = MasterPC->GetPawn())
    {
        QueryParams.AddIgnoredActor(MasterPawn->GetUniqueID());
    }

    FHitResult HitResult;
    const FVector FarPoint = ViewRotation.Vector() * 10000.f;
    const bool IsOverlap = GetWorld()->LineTraceSingleByChannel(HitResult, ViewPoint, FarPoint, ECC_Visibility, QueryParams);

    if (IsOverlap)
    {
        LookAtPoint = HitResult.ImpactPoint;
        return true;
    }
    
    LookAtPoint = FVector();
    return false;    
}

void AGATargetGroundSelect::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
    FVector LookPoint;
    GetPlayerLookPoint(LookPoint);

    bool DebugEnabled = false;
    if (DebugEnabled)
    {
        UE_LOG(LogTemp, Display, TEXT("%f %f %f"), LookPoint.X, LookPoint.Y, LookPoint.Z);
        DrawDebugSphere(GetWorld(), LookPoint, Radius, 32, FColor::Red, false, 0.f, 3.f);
    }

    Decal->SetWorldLocation(LookPoint);
}