// Fill out your copyright notice in the Description page of Project Settings.


#include "AttributeSetBase.h"

#include <GameplayEffect.h>
#include <GameplayEffectExtension.h>

UAttributeSetBase::UAttributeSetBase()
    : Health(200.f)
    , MaxHealth(200.f)
    , Mana(100.f)
    , MaxMana(100.f)
    , Strength(250.f)
    , MaxStrength(250.f)
{
}

void UAttributeSetBase::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
    if (Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<FProperty>(UAttributeSetBase::StaticClass(), GET_MEMBER_NAME_CHECKED(UAttributeSetBase, Health)))
    {
        ChangeAttribute(Health, MaxHealth, OnHealthChange);
    }
    else if (Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<FProperty>(UAttributeSetBase::StaticClass(), GET_MEMBER_NAME_CHECKED(UAttributeSetBase, Mana)))
    {
        ChangeAttribute(Mana, MaxMana, OnManaChange);
    }
    else if (Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<FProperty>(UAttributeSetBase::StaticClass(), GET_MEMBER_NAME_CHECKED(UAttributeSetBase, Strength)))
    {
        ChangeAttribute(Strength, MaxStrength, OnStrengthChange);
    }
}

void UAttributeSetBase::ChangeAttribute(FGameplayAttributeData& Attribute, FGameplayAttributeData& MaxAttribute, FOnAttributeChangeDelegate& OnChange)
{
    Attribute.SetCurrentValue(FMath::Clamp(Attribute.GetCurrentValue(), 0.f, MaxAttribute.GetCurrentValue()));
    Attribute.SetBaseValue(FMath::Clamp(Attribute.GetBaseValue(), 0.f, MaxAttribute.GetCurrentValue())); // Current max? Not base?

    OnChange.Broadcast(Attribute.GetCurrentValue(), MaxAttribute.GetCurrentValue());
}