// Fill out your copyright notice in the Description page of Project Settings.


#include "GATargetGetAround.h"

#include <Abilities/GameplayAbility.h>
#include <GameFramework/Pawn.h>
#include <GameFramework/PlayerController.h>

void AGATargetGetAround::StartTargeting(UGameplayAbility* Ability)
{
    Super::StartTargeting(Ability);
    MasterPC = Cast<APlayerController>(Ability->GetOwningActorFromActorInfo()->GetInstigatorController());
}

void AGATargetGetAround::ConfirmTargetingAndContinue()
{
    APawn* OwningPawn = MasterPC->GetPawn();
    if (!OwningPawn)
    {
        TargetDataReadyDelegate.Broadcast(FGameplayAbilityTargetDataHandle());
        return;
    }

    FVector ViewLocation = OwningPawn->GetActorLocation();
    TArray<FOverlapResult> Overlaps;
    TArray<TWeakObjectPtr<AActor>> OverlapedActor; 
    bool IsTraceComplex = false;

    FCollisionQueryParams CollisionQueryParams;
    CollisionQueryParams.bTraceComplex = IsTraceComplex;
    CollisionQueryParams.bReturnPhysicalMaterial = false;
    if (APawn* MasterPawn = MasterPC->GetPawn())
    {
        CollisionQueryParams.AddIgnoredActor(MasterPawn->GetUniqueID());
    }

    const bool IsOverlap = GetWorld()->OverlapMultiByObjectType(Overlaps, 
                                                                ViewLocation, 
                                                                FQuat::Identity, 
                                                                FCollisionObjectQueryParams(ECC_Pawn), 
                                                                FCollisionShape::MakeSphere(Radius), 
                                                                CollisionQueryParams);
    if (IsOverlap)
    {
        for (int32 o = 0; o < Overlaps.Num(); ++o)
        {
            if (APawn* OverlapedPawn = Cast<APawn>(Overlaps[o].GetActor()))
            {
                OverlapedActor.AddUnique(OverlapedPawn);
            }
        }
    }

    if (OverlapedActor.Num() > 0)
    {
        FGameplayAbilityTargetDataHandle TargetData = StartLocation.MakeTargetDataHandleFromActors(OverlapedActor);
        TargetDataReadyDelegate.Broadcast(TargetData);
    }
    else
    {
        TargetDataReadyDelegate.Broadcast(FGameplayAbilityTargetDataHandle());
    }
}