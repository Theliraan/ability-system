// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityTypes.h"

FGameplayAbilityInfo::FGameplayAbilityInfo() 
	: CooldownDuration(0)
	, Cost(0)
	, CostType(EAbilityCostType::Mana)
	, UIMaterial(nullptr)
	, Class(nullptr)
{}

FGameplayAbilityInfo::FGameplayAbilityInfo(
	float InCooldownDuration, 
	float InCost, 
	EAbilityCostType InCostType, 
	UMaterialInstance* InUIMaterial,
	TSubclassOf<UGameplayAbilityBase> InClass)	
	: CooldownDuration(InCooldownDuration)
	, Cost(InCost)
	, CostType(InCostType)
	, UIMaterial(InUIMaterial)
	, Class(InClass)
{}