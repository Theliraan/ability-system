// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayAbilityBase.h"

#include <GameplayEffect.h>

FGameplayAbilityInfo UGameplayAbilityBase::GetAbilityInfo() const
{
    UGameplayEffect* CooldownEffect = GetCooldownGameplayEffect();
    UGameplayEffect* CostEffect = GetCostGameplayEffect();
    if (CooldownEffect && CostEffect && CostEffect->Modifiers.Num() > 0)
    {
        float CooldownDuration = 0;
        CooldownEffect->DurationMagnitude.GetStaticMagnitudeIfPossible(1.f, CooldownDuration);

        float Cost = 0;
        EAbilityCostType CostType = EAbilityCostType::Mana;

        FGameplayModifierInfo EffectInfo = CostEffect->Modifiers[0];
        EffectInfo.ModifierMagnitude.GetStaticMagnitudeIfPossible(1.f, Cost);

        FGameplayAttribute CostAttribute = EffectInfo.Attribute;
        FString CostAttributeName = CostAttribute.AttributeName;
        if (CostAttributeName == "Health")
        {
            CostType = EAbilityCostType::Health;
        }
        else if (CostAttributeName == "Strength")
        {
            CostType = EAbilityCostType::Strength;
        }

        return FGameplayAbilityInfo(CooldownDuration, Cost, CostType, UIMaterial, GetClass());
    }

    return FGameplayAbilityInfo();
}
